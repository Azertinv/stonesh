# README #

StoneSh is a command interpreter for keystone and capstone designed to be an alternative to ks/cstool and nasm-shell.

## How do I get set up ? ##

* [Install keystone's python binding](https://github.com/keystone-engine/keystone)
* [Install capstone's python binding](https://github.com/aquynh/capstone)

## What's in it ? ##

Keystone
```
x32> a xor eax, eax; call 0xc1040a9d; call 0xc1040bbb; ret
\x31\xc0\xe8\x96\xfa\x03\xc1\xe8\xaf\xfb\x03\xc1\xc3
```

Capstone
```
x32> d 31c0e896fa03c1e8affb03c1c3
0x1000:	xor	eax, eax
0x1002:	call	-0x3efbf563
0x1007:	call	-0x3efbf445
0x100c:	ret	
```

Easy output formatting
```
x32> get format
format: \x{:02x}
x32> a xor eax, eax
\x31\xc0
x32> set format {:02x}
x32> a xor eax, eax
31c0
```
```
x32> set asm_prefix shellcode = "
x32> set asm_suffix "
x32> a xor eax, eax; nop
shellcode = "\x31\xc0\x90"
```

Multiple architectures
```
x32> a xor rax, rax
Invalid operand (KS_ERR_ASM_INVALIDOPERAND)
x32> set arch x64
x64> a xor rax, rax
\x48\x31\xc0
```

File support
```
$ echo "xor eax, eax" > ayy
$ stonesh
x32> a @ayy
\x31\xc0
```

Autocompletion (might explode in your face)
```
x32> a @/
bin         etc         lib         media       proc        sbin        tmp         vmlinuz     
boot        home        lib64       mnt         root        srv         usr         
dev         initrd.img  lost+found  opt         run         sys         var         
```
