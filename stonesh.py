#!/usr/bin/python

import sys
import os
import cmd
import pprint

from capstone import *
from keystone import *

# TODO add more archs
archs = {"x16": (KS_ARCH_X86, KS_MODE_16, CS_ARCH_X86, CS_MODE_16),
         "x32": (KS_ARCH_X86, KS_MODE_32, CS_ARCH_X86, CS_MODE_32),
         "x64": (KS_ARCH_X86, KS_MODE_64, CS_ARCH_X86, CS_MODE_64),
        }

FILE_PREFIX = '@'

class StoneShCmd(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.aliases = {'dis':  self.do_d,
                        'asm':  self.do_a,
                        'q':    self.do_quit,
                        }

        self.cfg = {"start_addr":   0x1000,
                    "prompt":       "{arch}>",
                    "arch":         "x32",
                    "format":       "\\x{:02x}",
                    "asm_prefix":   "",
                    "asm_suffix":   "",}

        self.reload_config()

########################### CONFIG
    def reload_engines(self):
        if self.cfg["arch"] in archs:
            ks_arch, ks_mode, cs_arch, cs_mode = archs[self.cfg["arch"]]
            self.ks = Ks(ks_arch, ks_mode)
            self.cs = Cs(cs_arch, cs_mode)
        else:
            print "Invalid arch"
            self.cfg["arch"] = "x32"
            self.reload_config()

    def reload_config(self):
        self.prompt = self.cfg["prompt"].format(arch=self.cfg["arch"]) + " "
        self.reload_engines()

    def do_config(self, line):
        pprint.pprint(self.cfg)

    def do_get(self, line):
        option, _, _ = self.parseline(line) 
        if option in self.cfg.keys():
            print "{}: {}".format(option, str(self.cfg[option]))
        else:
            print "no such option: " + str(option)

    def complete_get(self, text, line, begidx, endidx):
        return [x for x in self.cfg.keys() if x[:len(text)] == text]

    def do_set(self, line):
        # TODO FIX hex value parsing
        option, value, _ = self.parseline(line) 
        try:
            if option in self.cfg.keys():
                self.cfg[option] = type(self.cfg[option])(value)
                self.reload_config()
            else:
                print "no such option: " + str(option)
        except ValueError as e:
            print "Invalid option value: " + str(value)
            print "Need " + str(type(self.cfg[option]))

    def complete_set(self, text, line, begidx, endidx):
        # TODO autocomp params
        # TODO FIX autocomp when pressing tab on value
        return [x for x in self.cfg.keys() if x[:len(text)] == text]

########################### DISASSEMBLY
    def disasm(self, code):
        for e in self.cs.disasm(code, self.cfg["start_addr"]):
            print hex(int(e.address)) + ":\t"+ e.mnemonic + "\t" + e.op_str

    def do_d(self, line):
        """disasm DATA\ndisassemble the DATA with the arch set in the config"""
        try:
            if line[0] == FILE_PREFIX:
                with open(line[1:], 'r') as f:
                    self.disasm(f.read())
            else:
                self.disasm(bytearray.fromhex(line))
        except IndexError as e:
            print "disasm take 1 argument, 0 provided"
        except IOError as e:
            print e
        except ValueError as e:
            print "non hexadecimal value provided"

    def complete_d(self, text, line, begidx, endidx):
        if FILE_PREFIX in line:
            return self.autocomp_file(line[line.index(FILE_PREFIX) + 1:])
        return []

########################### ASSEMBLY
    def asm(self, code):
        sys.stdout.write(self.cfg["asm_prefix"])
        for e in self.ks.asm(code, self.cfg["start_addr"])[0]:
            sys.stdout.write(self.cfg["format"].format(e))
        sys.stdout.write(self.cfg["asm_suffix"])
        sys.stdout.write("\n")

    def do_a(self, line):
        """asm CODE\nassemble the CODE in the arch set in the config"""
        try:
            if line[0] == FILE_PREFIX:
                with open(line[1:], 'r') as f:
                    self.asm(f.read())
            else:
                self.asm(line)
        except IndexError as e:
            print "asm take 1 argument, 0 provided"
        except IOError as e:
            print e
        except KsError as e:
            print e

    def complete_a(self, text, line, begidx, endidx):
        # TODO complete instructions depending on arch
        if FILE_PREFIX in line:
            return self.autocomp_file(line[line.index(FILE_PREFIX) + 1:])
        return []

########################### UTILS
    def autocomp_file(self, name):
        # http://stackoverflow.com/questions/4001708/change-how-python-cmd-module-handles-autocompletion
        # TODO FIX autocomp when filename has special character
        if os.path.isdir(name):
            d = name
            f = ''
        else:
            d = os.path.dirname(name)
            f = os.path.basename(name)
        if len(d) == 0:
            d = os.path.curdir
        if len(f) == 0:
            f = ''
        if os.path.isdir(d):
            return [x for x in os.listdir(d) if x[:len(f)] == f]
        return []

    def do_quit(self, line):
        return True

    def do_EOF(self, line):
        print ""
        return True

    def default(self, line):
        cmd, arg, line = self.parseline(line)
        if cmd in self.aliases:
            return (self.aliases[cmd](arg))
        else:
            print "command not found: " + cmd

if __name__ == "__main__":
    ### TODO argparse (arch)
    ### TODO config (~/.stonesh)
    sh = StoneShCmd()
    sh.cmdloop()
